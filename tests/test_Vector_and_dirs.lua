lu = require('luaunit')

kas    = kas    or dofile('lib/kas.lua')
Vector = Vector or dofile('lib/Vector.lua')
dirs   = dirs   or dofile('lib/dirs.lua')

-- unpack dirs into the global namespace
for k, v in pairs(dirs) do
    _G[k] = v
end


local tests = {}
    tests.v1 = Vector:new(1,2,3)
    tests.v2 = Vector:new(3,2,1)

    function tests.test_defaults()
        local v = Vector:new()
        lu.assertEquals(v, Vector:new(0,0,0))

        v = Vector:new(1)
        lu.assertEquals(v, Vector:new(1,0,0))

        v = Vector:new(1,2)
        lu.assertEquals(v, Vector:new(1,2,0))

        v = Vector:new(1,2,3)
        lu.assertEquals(v, Vector:new(1,2,3))
    end

    function tests.test_eq()
        lu.assertEquals(tests.v1, Vector:new(1,2,3))
        lu.assertNotEquals(tests.v1, Vector:new(1,1,3))
        lu.assertNotEquals(tests.v1, Vector:new(1,2,2))
        lu.assertNotEquals(tests.v1, Vector:new(2,2,3))
    end

    function tests.test_unm()
        lu.assertEquals(-tests.v1, Vector:new(-1,-2,-3))
        lu.assertEquals(-UP, DOWN)
        lu.assertEquals(-EAST, WEST)
    end

    function tests.test_copy()
        local v1c = tests.v1:copy()
        lu.assertEquals(v1c, tests.v1)

        v1c.x = 3
        v1c.y = 2
        v1c.z = 1
        lu.assertEquals(tests.v1, Vector:new(1,2,3))
        lu.assertEquals(v1c, Vector:new(3,2,1))
    end

    function tests.test_rotate()
        lu.assertEquals(NORTH:rotate(90), EAST)
        lu.assertEquals(SOUTH:rotate(180), NORTH)
        lu.assertEquals(WEST:rotate(-90), SOUTH)

        -- :rotate() operates on the yaw axis, so UP and DOWN should be
        -- unaffected
        for _, v in ipairs{0, 90, 180, 270} do
            lu.assertEquals(UP:rotate(v), UP)
            lu.assertEquals(DOWN:rotate(v), DOWN)
        end

        lu.assertEquals(tests.v1:rotate(-180), tests.v1:rotate(180))
        lu.assertEquals(tests.v1:rotate(-90), tests.v1:rotate(270))
        lu.assertEquals(tests.v1:rotate(-270), tests.v1:rotate(90))

        lu.assertEquals(tests.v1:rotate(90):rotate(90), tests.v1:rotate(180))
        lu.assertEquals(tests.v1:rotate(-90):rotate(90), tests.v1)
        lu.assertEquals(tests.v1:rotate(90):rotate(180):rotate(-90):rotate(180), tests.v1)
        lu.assertEquals(tests.v1:rotate(0), tests.v1)

        lu.assertErrorMsgContains(
            'degrees must be divisible by 90!',
            tests.v1.rotate, tests.v1, 89)

        lu.assertErrorMsgContains(
            'degrees must be divisible by 90!',
            tests.v1.rotate, tests.v1, 34)

        lu.assertErrorMsgContains(
            'degrees must be divisible by 90!',
            tests.v1.rotate, tests.v1, -8)
    end

    function tests.test_operators()
        lu.assertEquals(tests.v1 + tests.v2, Vector:new(4,4,4))
        lu.assertEquals(tests.v1 - tests.v2, Vector:new(-2,0,2))
        lu.assertEquals(tests.v2 - tests.v1, Vector:new(2,0,-2))
        lu.assertEquals(tests.v1 * Vector:new(0,1,0), Vector:new(0,2,0))
        lu.assertEquals(tests.v1 * Vector:new(1,0,1), Vector:new(1,0,3))
        lu.assertEquals(tests.v1 / tests.v1, Vector:new(1,1,1))
        lu.assertEquals(tests.v1 / Vector:new(1/2, 2/2, 3/2), Vector:new(2,2,2))
    end
-- end of tests table

return tests
