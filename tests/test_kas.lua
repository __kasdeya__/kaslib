lu = require('luaunit')

kas = kas or dofile('lib/kas.lua')

local tests = {}
    function tests.test_indexOf()
        lu.assertEquals(kas.indexOf('asdf', {'foo', 'bar', 'asdf'}), 3)
        lu.assertNil(kas.indexOf(0, {1, 2, 3}))

        local t = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

        for i=1,10 do
            lu.assertEquals(kas.indexOf(i, t), i)
        end
    end

    function tests.test_tableCount()
        lu.assertEquals(kas.tableCount(2, {1,1,2,2,1,2,1}), 3)
        lu.assertEquals(kas.tableCount(2, {1,1,1,1,1,1}), 0)
        lu.assertEquals(kas.tableCount('foo', {'baz', 'qux', 'quux'}), 0)
        lu.assertEquals(kas.tableCount('foo', {'foo', 'bar', 'baz', 'qux', 'foo', 'quux'}), 2)
    end

    function tests.test_tableEq()
        lu.assertFalse(kas.tableEq({}, {1}))
        lu.assertFalse(kas.tableEq({1,1}, {1}))
        lu.assertFalse(kas.tableEq({1,1}, {1,2}))
        lu.assertFalse(kas.tableEq({1,2}, {1,"2"}))
        lu.assertTrue(kas.tableEq({}, {}))
        lu.assertTrue(kas.tableEq({"foo", "bar"}, {"foo", "bar"}))
    end

    function tests.test_setInsert()
        local t = {2, 4, 6, 8, 10}

        for i=1,10 do
            local success = kas.setInsert(t, i)
            lu.assertEquals(success, (i%2 == 1))
            assert(kas.indexOf(i, t))
        end
    end

    function tests.test_round()
        lu.assertEquals(kas.round(-0.9), -1)
        lu.assertEquals(kas.round(0.5), 1)
        lu.assertEquals(kas.round(0.4), 0)
    end

    function tests.test_clamp()
        lu.assertEquals(kas.clamp(1.9, 1.7, 1.8), 1.8)
        lu.assertEquals(kas.clamp(2, 3, 4), 3)
        lu.assertEquals(kas.clamp(3.1, 2, 4), 3.1)
    end

    function tests.test_precisionRound()
        lu.assertEquals(kas.precisionRound(math.pi, 1), 3.1)
        lu.assertEquals(kas.precisionRound(math.pi, 2), 3.14)
        lu.assertEquals(kas.precisionRound(math.pi, 3), 3.142)
    end

    function tests.test_divmod()
        local div, mod = kas.divmod(64*9 + 12, 64)
        lu.assertEquals(div, 9)
        lu.assertEquals(mod, 12)
    end

    function tests.test_copyTable()
        local t = {1, 2, 3, 4, 5, {6}}
        local t_ = kas.copyTable(t)

        for i=1,#t do
            lu.assertEquals(t[i], t_[i])
        end
    end

    function tests.test_split()
        local t  = kas.split('foo,,bar,baz,,,qux', ',')
        local t_ = kas.split('foo  bar baz \t  qux')
        local correct_t = {'foo', 'bar', 'baz', 'qux'}

        for i=1,#correct_t do
            lu.assertEquals(t[i], correct_t[i])
            lu.assertEquals(t_[i], correct_t[i])
        end
    end

    function tests.test_getHostType()
        local oldHost = _HOST
        local oldOsVer = _OSVERSION

        _G._HOST = nil
        _G._OSVERSION = nil
        lu.assertEquals(kas.getHostType(), nil)

        _G._HOST = 'ComputerCraft 1.33.7 (Minecraft 1.33.7)'
        lu.assertEquals(kas.getHostType(), 'ComputerCraft')
        _G._HOST = nil

        _G._OSVERSION = "OpenOS 1.33.7"
        lu.assertEquals(kas.getHostType(), "OpenComputers")

        _G._HOST = oldHost
        _G._OSVERSION = oldOsVer
    end

    function tests.test_spaceship()
        lu.assertEquals(kas.spaceship(99999, 0), 1)
        lu.assertEquals(kas.spaceship(math.pi, 4), -1)
        lu.assertEquals(kas.spaceship(math.pi, -111), 1)
        lu.assertEquals(kas.spaceship(math.pi, math.pi), 0)
        lu.assertEquals(kas.spaceship(1337, 1337), 0)
    end
-- end of tests table

return tests
