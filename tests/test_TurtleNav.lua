lu = require('luaunit')

kas       = kas       or dofile('lib/kas.lua')
TurtleNav = TurtleNav or dofile('lib/TurtleNav.lua')
Vector    = Vector    or dofile('lib/Vector.lua')
dirs      = dirs      or dofile('lib/dirs.lua')

-- unpack dirs into the global namespace
-- (TurtleNav does this already but it doesn't hurt to be thorough)
for k, v in pairs(dirs) do
    _G[k] = v
end


-- yesTurt is a very simple turtle API emulator
-- every unset key in yesTurt leads to a function that always returns true, and
-- logs the function call
local yesTurt_mt = {
    __index = function (self, key)
        local mt = getmetatable(self)

        if mt[key] ~= nil then
            return mt[key]
        end

        return function ()
            table.insert(self.calls, key)
            return true
        end
    end,

    calls = {},
}

local yesTurt = {}
setmetatable(yesTurt, yesTurt_mt)


local tests = {}
    tests.yesTurt = yesTurt
    tests.oldTurt = _G.turtle

    function tests:setUp()
        _G.turtle = self.yesTurt

        self.nav = TurtleNav:new()
    end

    function tests:tearDown()
        self:resetCalls()
        _G.turtle = self.oldTurt
    end

    function tests:assert180Turn()
        -- assert that TurtleNav has done a 180-degree turn, and nothing else.
        -- this function automatically runs self:resetCalls()
        local turnDir = self.yesTurt.calls[1]
        lu.assertTrue(turnDir == 'turnLeft' or turnDir == 'turnRight')
        lu.assertEquals(self.yesTurt.calls, {turnDir, turnDir})
        self:resetCalls()
    end

    function tests:assertNCalls(call, n)
        -- assert that there are `n` calls matching the string `call`
        local nCalls = kas.tableCount(call, turtle.calls)

        if nCalls ~= n then
            -- TODO: pretty-print call log here
            error(string.format(
                'expecting %s calls to "%s" in yesTurt call log',
                nCalls, call))
        end
    end

    function tests:assertStateEquals(pos, heading, calls)
        -- can set any argument to nil to skip that check
        --
        -- this method automatically runs self:resetCalls() if `calls` is not
        -- nil
        --
        -- `calls` can be set to the special value `180` to automatically run
        -- assert180Turn()
        --
        -- this method is mostly just here to make the test code a little more
        -- concise and readable

        if pos ~= nil then
            lu.assertEquals(self.nav.pos, pos)
        end

        if heading ~= nil then
            lu.assertEquals(self.nav.heading, heading)
        end

        if calls == 180 then
            self:assert180Turn()
        elseif calls ~= nil then
            lu.assertEquals(self.yesTurt.calls, calls)
            self:resetCalls()
        end
    end

    function tests:resetCalls()
        -- reset yesTurt's call table
        self.yesTurt.calls = {}
    end


    function tests:test_nav_defaults()
        self:assertStateEquals(Vector:new(0,0,0), NORTH, {})
    end

    function tests:test_turn_relative()
        local sv = Vector:new(0,0,0) -- starting vector

        lu.assertTrue( self.nav:turnLeft() )
        self:assertStateEquals(sv, WEST, {'turnLeft'})

        lu.assertTrue( self.nav:turnAround() )
        self:assertStateEquals(sv, EAST, 180)

        lu.assertTrue( self.nav:turnRight() )
        self:assertStateEquals(sv, SOUTH, {'turnRight'})

        lu.assertTrue( self.nav:turn(FORWARD) )
        self:assertStateEquals(sv, SOUTH, {})

        lu.assertTrue( self.nav:turn(BACK) )
        self:assertStateEquals(sv, NORTH, 180)

        lu.assertTrue( self.nav:turn(LEFT) )
        self:assertStateEquals(sv, WEST, {'turnLeft'})

        lu.assertTrue( self.nav:turn(RIGHT) )
        self:assertStateEquals(sv, NORTH, {'turnRight'})
    end

    function tests:test_turn_absolute()
        local sv = Vector:new(0,0,0) -- starting vector

        self.nav:turn(WEST)
        self:assertStateEquals(sv, WEST, {'turnLeft'})

        self.nav:turn(NORTH)
        self:assertStateEquals(sv, NORTH, {'turnRight'})

        self.nav:turn(EAST) 
        self:assertStateEquals(sv, EAST, {'turnRight'})

        self.nav:turn(SOUTH)
        self:assertStateEquals(sv, SOUTH, {'turnRight'})

        self.nav:turn(NORTH)
        self:assertStateEquals(sv, NORTH, 180)

        self.nav:turn(WEST) 
        self:assertStateEquals(sv, WEST, {'turnLeft'})

        self.nav:turn(EAST)
        self:assertStateEquals(sv, EAST, 180)
    end

    function tests:test_go_relative()
        lu.assertTrue( self.nav:forward(2, true) )
        self:assertStateEquals(
            Vector:new(0,0,2), NORTH, {'dig', 'forward', 'dig', 'forward'})

        lu.assertTrue( self.nav:back(2, false) )
        local turnDir = self.yesTurt.calls[1]
        lu.assertTrue(turnDir == 'turnLeft' or turnDir == 'turnRight')
        self:assertStateEquals(
            Vector:new(0,0,0), SOUTH, {turnDir, turnDir, 'forward', 'forward'})
    end

    function tests:test_go_vertical()
        lu.assertTrue( self.nav:up(4, true) )
        self:assertStateEquals(
            Vector:new(0,4,0), NORTH,
            {'digUp', 'up', 'digUp', 'up', 'digUp', 'up', 'digUp', 'up'})

        lu.assertTrue( self.nav:down(4, true) )
        self:assertStateEquals(
            Vector:new(0,0,0), NORTH,
            {'digDown', 'down', 'digDown', 'down', 'digDown', 'down',
             'digDown', 'down'})
    end

    function tests:test_go_compass()
        lu.assertTrue( self.nav:east(1, true) )
        self:assertStateEquals(
            Vector:new(1,0,0), EAST, {'turnRight', 'dig', 'forward'})

        lu.assertTrue( self.nav:north(1) )
        self:assertStateEquals(
            Vector:new(1,0,1), NORTH, {'turnLeft', 'forward'})

        lu.assertTrue( self.nav:west(1, false) )
        self:assertStateEquals(
            Vector:new(0,0,1), WEST, {'turnLeft', 'forward'})

        lu.assertTrue( self.nav:south(1, true) )
        self:assertStateEquals(
            Vector:new(0,0,0), SOUTH, {'turnLeft', 'dig', 'forward'})
    end

    function tests:test_absMove()
        -- test error on invalid coordName
        lu.assertErrorMsgContains(
            "coordName wasn't 'x', 'y', or 'z'",
            self.nav.absMove,
            self.nav, 'd', 2, true)
        self:assertStateEquals(Vector:new(0,0,0), NORTH, {})

        lu.assertTrue( self.nav:absMove('x', 2, true) )
        self:assertStateEquals(
            Vector:new(2,0,0), EAST,
            {'turnRight', 'dig', 'forward', 'dig', 'forward'})

        lu.assertTrue( self.nav:absMove('y', -1, true) )
        self:assertStateEquals(Vector:new(2,-1,0), EAST, {'digDown', 'down'})

        lu.assertTrue( self.nav:absMove('z', -1) )
        self:assertStateEquals(
            Vector:new(2,-1,-1), SOUTH,
            {'turnRight', 'forward'})

        lu.assertTrue( self.nav:gotoX(0, true) )
        self:assertStateEquals(
            Vector:new(0,-1,-1), WEST,
            {'turnRight', 'dig', 'forward', 'dig', 'forward'})

        lu.assertTrue( self.nav:gotoY(0, true) )
        self:assertStateEquals(Vector:new(0,0,-1), WEST, {'digUp', 'up'})

        lu.assertTrue( self.nav:gotoZ(1, false) )
        self:assertStateEquals(
            Vector:new(0,0,1), NORTH, {'turnRight', 'forward', 'forward'})
    end
-- end of tests table

return tests
