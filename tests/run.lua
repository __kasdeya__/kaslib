lu = require('luaunit')

test_Vector_and_dirs = dofile('tests/test_Vector_and_dirs.lua')
test_kas             = dofile('tests/test_kas.lua')
test_TurtleNav       = dofile('tests/test_TurtleNav.lua')

os.exit( lu.LuaUnit.run() )
