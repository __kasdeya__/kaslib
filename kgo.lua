local USAGE_STRING = [[
Usage: kgo <movement command>...

A simple command-line tool for efficiently maneuvering turtles. For every character in the arguments provided, the turtle will move according to this table:

character | operation
----------+----------
        f | turtle.forward
        b | turtle.back
        u | turtle.up
        d | turtle.down
        l | turtle.turnLeft
        r | turtle.turnRight

Spaces are ignored completely.

Examples:

kgo ffff l f r
kgo uuu ff dd
kgo ufufullfr
]]

function usage()
    print(USAGE_STRING)
end

local args = {...}

if #args <= 0 then
    usage()
    os.exit(1)
end

local s = ''
for _, arg in ipairs(args) do
    s = s .. arg
end

for i=1,#s do
    local c = s:sub(i, i)

    if c == 'f' then
        turtle.forward()
    elseif c == 'b' then
        turtle.back()
    elseif c == 'l' then
        turtle.turnLeft()
    elseif c == 'r' then
        turtle.turnRight()
    elseif c == 'u' then
        turtle.up()
    elseif c == 'd' then
        turtle.down()
    else
        error('unrecognized direction: ' .. c)
    end
end
