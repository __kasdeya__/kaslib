# kaslib

A set of libraries for ComputerCraft, including a WIP turtle navigation system.

OpenComputers support is planned for the future!


## Description

kaslib provides a set of libraries designed to simplify common tasks in ComputerCraft. `kas.lua` provides a set of miscellaneous functions designed to make life easier. `Vector.lua` provides a `Vector` class for keeping track of coordinates in the world. `TurtleNav.lua` provides a simple navigation system for turtles, allowing them to automatically keep track of their position and heading.


## Installing

To install kaslib into a ComputerCraft computer:

1. Copy the `lib` directory to the root of the computer's filesystem.

2. Optionally, copy `startup.lua` to the root of the computer's filesystem. This will allow you to pretty-print things from the REPL by running `dir()` on them.

3. Optionally, copy `kgo.lua` to the root of the computer's filesystem. This will allow you to use the `kgo` command to efficiently move turtles.

4. That's it! See *Usage* below for information on how to import the kaslib libraries.


## Usage

To import kaslib libraries into your code, simply include one or more of these lines, as needed:

    kas = kas or dofile('/lib/kas.lua')
    Vector = Vector or dofile('/lib/Vector.lua')
    dirs = dirs or dofile('/lib/dirs.lua')
    TurtleNav = TurtleNav or dofile('/lib/TurtleNav.lua')

Note that this is different from the usual `os.loadAPI()`. This is because kaslib is designed to be platform-agnostic where possible, and `os.loadAPI()` is unique to ComputerCraft.


## Generating documentation

### On Linux

1. Ensure that Lua 5.x and luarocks are both installed. You should be able to do this using your system's package manager, using a command like `sudo apt install lua luarocks`.

2. Use luarocks to install LDoc, using the command: `sudo luarocks install ldoc`

3. `cd` to this repository and run `ldoc .`.

Once generated, you can view the documentation by opening `doc/index.html` in any web browser.

### On Windows

Consider installing WSL and following the "On Linux" instructions above. (Through a WSL terminal, of course.)

Otherwise, good luck.


## Future goals

- <!-- TODO --> Create an autorun file so that this entire repo can be copied to a floppy disk verbatim, then installed in a ComputerCraft computer automatically.
- <!-- TODO --> Unittest TurtleNav's turtle API error handling.
- <!-- TODO --> Add example code. (quarry, auto-farmer, minecart tunnel maker, etc.)
- <!-- TODO --> All direction constants should have decent tostring() output. (NamedConstant class for relative dirs)
- <!-- TODO --> Host documentation online somewhere. (Read the Docs?)
- <!-- TODO --> Long term goal: Expand TurtleNav to keep track of all blocks it's 'seen', storing them in a database. Use this database for automatic pathfinding.


## License

kaslib uses the GNU GPLv3. See LICENSE for more information.
