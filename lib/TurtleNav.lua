-------------------------------------------------------------------------------
-- A simple navigation system for turtles.
--
-- Keeps track of a turtle's coordinates and heading, and uses this
-- information to provide a convenient layer of abstraction.
--
-- @module TurtleNav
-- @author kasdeya
-- @license GNU GPLv3
-- @copyright kasdeya 2021

kas    = kas    or dofile('lib/kas.lua')
Vector = Vector or dofile('lib/Vector.lua')
dirs   = dirs   or dofile('lib/dirs.lua')

-- unpack dirs into the global namespace
for k, v in pairs(dirs) do
    _G[k] = v
end


--- @type TurtleNav
local TurtleNav = {}
TurtleNav.__index = TurtleNav


--- TurtleNav's constructor.
--
-- @function TurtleNav:new
--
-- @tparam[opt] Vector pos
-- The turtle's current coordinates. Defaults to (0,0,0)
-- @todo TODO: Find a way to display the default vector value correctly.
--
-- @tparam[opt=dirs.NORTH] Vector heading
-- The turtle's current heading, as a normalized Vector.
function TurtleNav.new(class, pos, heading)
    pos = pos or Vector:new(0,0,0)
    heading = heading or NORTH

    local self = {pos=pos, heading=heading}
    setmetatable(self, class)
    return self
end

function TurtleNav:__tostring()
    return string.format("TurtleNav(%s, %s)",
        tostring(self.pos),
        tostring(self.heading))
end

local function relTurn(self, direction)
    if direction ~= LEFT
        and direction ~= RIGHT
        and direction ~= FORWARD
        and direction ~= BACK
    then
        error("relTurn() can only turn LEFT, RIGHT, FORWARD, or BACK")
    end

    local turnFunc, turnAng
    if direction == FORWARD then
        return true
    elseif direction == LEFT then
        turnFunc = turtle.turnLeft
        turnAng = -90
    elseif direction == RIGHT then
        turnFunc = turtle.turnRight
        turnAng = 90
    elseif direction == BACK then
        local result, message = relTurn(self, RIGHT)

        if not result then
            return result, message
        end

        return relTurn(self, RIGHT)
    end

    local result, message = turnFunc()

    if result then
        self.heading = self.heading:rotate(turnAng)
    end

    return result, message
end

--- Turn in a given direction.
--
-- @param direction
-- The direction to turn in. (see: @{dirs}) If this is a compass direction,
-- turn to face it.
--
-- @treturn bool  `true` on success and `false` on failure.
-- @treturn ?string|nil `nil` on success. Otherwise, gives an error message.
--
-- @todo TODO: Document all errors that this can raise.
function TurtleNav:turn(direction)
    if direction == UP or direction == DOWN then
        error('attempted to turn() UP or DOWN')
    end

    -- if it's a relative direction, then pass it to relTurn
    -- (PRIMITIVE_DIRS also contains UP and DOWN but we checked for that
    -- already)
    if kas.indexOf(direction, PRIMITIVE_DIRS) then
        return relTurn(self, direction)
    end

    -- if we're already facing the right direction, do nothing
    if direction == self.heading then
        return true
    end

    -- otherwise, turn RIGHT, LEFT, or BACK as needed
    if direction == self.heading:rotate(90) then
        return relTurn(self, RIGHT)
    elseif direction == self.heading:rotate(-90) then
        return relTurn(self, LEFT)
    elseif direction == self.heading:rotate(180) then
        return relTurn(self, BACK)
    end
end

--- Travel in `direction` for `distance`, optionally digging.
--
-- @tparam Vector direction
-- A normalized Vector representing the direction to move in. See @{dirs}.
--
-- @int[opt=1] distance
--
-- @bool[opt=false] dig
-- If `true`, automatically dig any blocks that get in the way. (Note that this
-- can't handle falling blocks such as sand or gravel yet.)
function TurtleNav:go(direction, distance, dig)
    distance = distance or 1

    local result, message

    -- if we're not moving UP or DOWN, then turn so that the direction we want
    -- to move in is FORWARD
    --
    -- also, set `direction` to the absolute COMPASS_DIR of our current `heading`
    --
    -- this way, no matter what `direction` started off as, now it's a Vector
    -- that we can add to self.pos every time we successfully move, to keep
    -- our position updated
    if direction ~= UP and direction ~= DOWN then
        self:turn(direction)
        direction = self.heading
    end

    local digFunc, moveFunc
    if direction == UP then
        digFunc = turtle.digUp
        moveFunc = turtle.up
    elseif direction == DOWN then
        digFunc = turtle.digDown
        moveFunc = turtle.down
    else
        digFunc = turtle.dig
        moveFunc = turtle.forward
    end

    -- if we're not supposed to dig then replace digFunc with a dummy function
    -- that always reports success
    if not dig then
        digFunc = function () return true end
    end

    for i=1,distance do
        local result, message = digFunc()

        -- if we get an error from trying to dig, and it isn't just from a lack
        -- of block to dig, exit the function and pass the error along
        if not result and message ~= "Nothing to dig here" then
            return result, message
        end

        -- if moving fails then return the error. otherwise, update our pos
        -- and keep going
        result, message = moveFunc()
        if not result then return result, message end
        self.pos = self.pos + direction
    end

    return true
end

--- Move to an absolute coordinate.
--
-- Move the turtle so that `TurtleNav.pos[coordName] == coordVal`, without
-- changing the other two coordinates.
--
-- In other words, if you tell absMove which coordinate you want to change, it
-- will move the turtle so that its position matches that coordinate.
--
-- @string coordName  `'x'`, `'y'`, or `'z'`
-- @int coordVal
-- @param ... Passed to @{TurtleNav:go}
--
-- @return Same return values as @{TurtleNav:go}
--
-- @raise "coordName wasn't 'x', 'y', or 'z'"
--
-- @usage
-- local nav = TurtleNav:new()
-- nav:absMove('x', 3) -- move to x=3
-- nav.pos --> Vector(3,0,0)
-- nav:absMove('y', 2) -- move to y=2
-- nav.pos --> Vector(3,2,0)
-- nav:absMove('x', 0) -- move to x=0
-- nav.pos --> Vector(0,2,0)
function TurtleNav:absMove(coordName, coordVal, ...)
    if coordName ~= 'x' and coordName ~= 'y' and coordName ~= 'z' then
        error("coordName wasn't 'x', 'y', or 'z'")
    end

    -- convert coordVal to a relative coordVal, representing how far up or down
    -- the axis we need to move to reach it
    local relCoordVal = coordVal - self.pos[coordName]

    -- if we relCoordVal is 0, that means we don't need to move at all, so we
    -- can skip some unnecessary computation by returning true now.
    if relCoordVal == 0 then
        return true
    end

    -- build a normalized vector to represent the direction we want to move in
    local direction = Vector:new(0,0,0)
    direction[coordName] = kas.spaceship(relCoordVal, 0)

    return self:go(direction, math.abs(relCoordVal), ...)
end

-- --------- --
-- shortcuts --
-- --------- --

--- A shortcut for `TurtleNav:turn(LEFT)`
function TurtleNav:turnLeft() return self:turn(LEFT) end

--- A shortcut for `TurtleNav:turn(RIGHT)`
function TurtleNav:turnRight() return self:turn(RIGHT) end

--- A shortcut for `TurtleNav:turn(BACK)`
function TurtleNav:turnAround() return self:turn(BACK) end


--- A shortcut for `TurtleNav:go(FORWARD, ...)`
function TurtleNav:forward(...) return self:go(FORWARD, ...) end

--- A shortcut for `TurtleNav:go(BACK, ...)`
function TurtleNav:back(...) return self:go(BACK, ...) end

--- A shortcut for `TurtleNav:go(UP, ...)`
function TurtleNav:up(...) return self:go(UP, ...) end

--- A shortcut for `TurtleNav:go(DOWN, ...)`
function TurtleNav:down(...) return self:go(DOWN, ...) end


--- A shortcut for `TurtleNav:go(NORTH, ...)`
function TurtleNav:north(...) return self:go(NORTH, ...) end

--- A shortcut for `TurtleNav:go(SOUTH, ...)`
function TurtleNav:south(...) return self:go(SOUTH, ...) end

--- A shortcut for `TurtleNav:go(EAST, ...)`
function TurtleNav:east(...) return self:go(EAST, ...) end

--- A shortcut for `TurtleNav:go(WEST, ...)`
function TurtleNav:west(...) return self:go(WEST, ...) end


--- Move to a specific X coordinate.
-- A shortcut for `TurtleNav:absMove('x', ...)`
function TurtleNav:gotoX(...) return self:absMove('x', ...) end

--- Move to a specific Y coordinate.
-- A shortcut for `TurtleNav:absMove('y', ...)`
function TurtleNav:gotoY(...) return self:absMove('y', ...) end

--- Move to a specific Z coordinate.
-- A shortcut for `TurtleNav:absMove('z', ...)`
function TurtleNav:gotoZ(...) return self:absMove('z', ...) end


return TurtleNav
