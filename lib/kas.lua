-------------------------------------------------------------------------------
-- Provides various utility functions to make life easier.
--
-- @module kas
-- @author kasdeya
-- @license GNU GPLv3
-- @copyright kasdeya 2021

local kas = {}

--- Pretty-print the contents of a table.
--
-- Designed for use on the lua repl. Since Minecraft computer screens have very
-- limited space, dir() will automatically pause once the screen has been
-- filled up. When this happens, press enter to view more of the table.
--
-- @tparam table t
-- @treturn nil
function kas.dir(t)
    -- note: not unittested
    local height, width = term.getSize()
    local lineNum = 1

    for name, value in pairs(t) do
        local toPrint = name .. " -> "

        if type(value) == 'function' then
            toPrint = toPrint .. 'function'
        elseif type(value) == 'table' then
            toPrint = toPrint .. 'table'
        else
            toPrint = toPrint .. tostring(value)
        end

        write(toPrint)

        if lineNum >= width then
            io.read()
            lineNum = 0
        else
            print()
            lineNum = lineNum + 1
        end
    end
end

--- Find the index of the first matching element in a list.
--
-- @param e  The element to find.
-- @tparam table l  The table to look for it in.
--
-- @treturn ?int|nil
-- The index of the first element in `l` that is `==` to `e`, or `nil` if no
-- elements matched.
function kas.indexOf(e, l)
    for i, v in ipairs(l) do
        if e == v then
            return i
        end
    end

    return nil
end

--- Find the number of elements in a list that match a given element.
--
-- @param e  The element to find.
-- @tparam table l  The table to look for it in
--
-- @treturn int  The number of elements in `l` that are `==` to `e`.
function kas.tableCount(e, l)
    -- returns the number of elements in `l` that == `e`
    local c = 0

    for _, v in ipairs(l) do
        if v == e then
            c = c + 1
        end
    end

    return c
end

--- Shallow table comparison.
--
-- @tparam table t1
-- @tparam table t2
--
-- @treturn bool
-- `true` if the tables look equal. Else, `false.
function kas.tableEq(t1, t2)
    if #t1 ~= #t2 then
        return false
    end

    for i=1, #t1 do
        if t1[i] ~= t2[i] then
            return false
        end
    end

    return true
end

--- Insert a unique element into a list.
--
-- Insert an element into a list, (in place) but only if the list doesn't
-- already have a matching element.
--
-- @tparam table l  The list.
-- @param e  The element.
-- @treturn bool `true` for success or `false` for failure.
function kas.setInsert(l, e)
    if kas.indexOf(e, l) then
        return false
    end

    table.insert(l, e)
    return true
end

--- Round a number.
--
-- @number n
-- @treturn int
function kas.round(n)
    return math.floor(n + 0.5)
end

--- Force a number to remain in a range.
--
-- Ensures that `n` remains between `min` and `max` by "clamping" it between
-- these values. If `n` goes below `min`, `min` is returned instead. If `n`
-- goes above `max`, `max` is returned. Otherwise, `n` is returned as-is.
--
-- @number n
-- @number min
-- @number max
-- @treturn number
--
-- @usage
-- kas.clamp(  2, 1, 3) --> 2
-- kas.clamp(-99, 1, 3) --> 1
-- kas.clamp( 99, 1, 3) --> 3
function kas.clamp(n, min, max)
    if n > max then return max end
    if n < min then return min end
    return n
end

--- Round a number to a given precision.
--
-- Round `n` to `p` decimal places.
--
-- @number n
-- @int p
-- @treturn number
--
-- @usage
-- kas.precisionRound(1.55555, 3) --> 1.556
-- kas.precisionRound(1.55555, 2) --> 1.56
-- kas.precisionRound(1.55555, 4) --> 1.5556
-- kas.precisionRound(1.99999, 3) --> 2
function kas.precisionRound(n, p)
    local p_ = 10^p
    return kas.round(n * p_) / p_
end

--- Integer divide with remainder.
--
-- @number n
-- @number m
-- @treturn int  `n` divided by `m`, without the decimal part. (`n//m`)
-- @treturn int  `n` modulo `m`. (`n % m`)
function kas.divmod(n, m)
    return math.floor(n/m), n%m
end

--- Create a shallow copy of a table.
--
-- @tparam table t
-- @treturn table  A shallow copy of `t`.
function kas.copyTable(t)
    local t_ = {}
    for k, v in pairs(t) do
        t_[k] = v
    end

    return t_
end

--- Split a string into pieces.
--
-- If `sep` is unset, split the string by whitespace. Otherwise, split it by
-- the character(s) in `sep`.
--
-- Note that this implementation has a quirk. When faced with two or more
-- characters matching `sep`, it treats this as one long separator. See the
-- examples below for more information.
--
-- @string inputstr
-- @tparam[opt] string sep
-- @treturn {string,...}
--
-- @usage
-- kas.split('foo,;bar;baz,qux', ',;') --> {'foo', 'bar', 'baz', 'qux'}
-- kas.split('foo  bar baz \t  qux')   --> {'foo', 'bar', 'baz', 'qux'}
function kas.split(inputstr, sep)
    sep = sep or "%s"

    local t = {}

    for str in inputstr:gmatch("([^"..sep.."]+)") do
        table.insert(t, str)
    end

    return t
end

--- Present the user with a yes/no prompt.
--
-- @string prompt  The yes/no prompt.
--
-- @tparam ?bool|nil default
-- The default response. If the user presses enter without typing anything,
-- this is what will be returned. If this is set to `nil`, the user will be
-- continually presented with the `prompt` until they explicitly choose a
-- value by typing `y` or `n`.
--
-- @treturn bool  The user's response.
function kas.yesNo(prompt, default)
    -- note: not unittested
    assert(prompt ~= nil, 'no prompt entered')

    if default == nil then
        io.write(prompt .. ' [yn]')
    elseif default == true then
        io.write(prompt .. ' [Yn]')
    elseif default == false then
        io.write(prompt .. ' [yN]')
    else
        assert(false, "yesNo()'s default wasn't nil, false, or true")
    end

    local response = io.read():lower()

    -- if the user responded correctly, accept their answer
    if response == 'n' then
        return false
    elseif response == 'y' then
        return true
    end

    -- otherwise, go with the default
    if default ~= nil then
        return default
    end

    -- if there's no default, ask them again
    return yesNo(prompt, default)
end

--- Attempt to determine which mod (if any) we're running on.
--
-- Keep in mind that this function relies on the behavior of the mod's
-- operating system. Using a nonstandard operating system on a ComputerCraft or
-- OpenComputers computer might result in this function mistakenly returning
-- `nil`.
--
-- @treturn ?string|nil
-- Returns `"ComputerCraft"` for ComputerCraft, `"OpenComputers"` for
-- OpenComputers, and `nil` otherwise.
function kas.getHostType()
    if type(_HOST) == "string" and _HOST:find("^ComputerCraft") then
        return "ComputerCraft"
    end

    if type(_OSVERSION) == "string" and _OSVERSION:find("^OpenOS") then
        return "OpenComputers"
    end

    return nil
end

--- Return the results of x <=> y.
--
-- This is an implementation of the [spaceship
-- operator](https://en.wikipedia.org/wiki/Three-way_comparison) - AKA three-way
-- comparison.
--
-- @number x
-- @number y
-- @treturn int
-- This function returns `-1` if `x < y`, `0` if `x == y`, and `1` if `x > y`.
function kas.spaceship(x, y)
    if x < y then
        return -1
    elseif x > y then
        return 1
    else
        return 0
    end
end

return kas
