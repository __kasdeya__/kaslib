-------------------------------------------------------------------------------
-- A set of direction constants for use with turtles.
--
-- @module dirs
-- @alias d
-- @author kasdeya
-- @license GNU GPLv3
-- @copyright kasdeya 2021

Vector = Vector or dofile('lib/Vector.lua')

--- This table *is* the dirs library.
--
-- It's shown as a separate table due to a limitation of LDoc
--
-- @table d
--
-- @tfield Vector NORTH  A normalized Vector representing north.
-- @tfield Vector SOUTH  A normalized Vector representing south.
-- @tfield Vector EAST  A normalized Vector representing east.
-- @tfield Vector WEST  A normalized Vector representing west.
-- @tfield Vector UP  A normalized Vector representing up.
-- @tfield Vector DOWN  A normalized Vector representing down.
--
-- @tfield table FORWARD
-- An empty table representing forward, relative to the turtle's heading.
--
-- @tfield table BACK
-- An empty table representing backwards, relative to the turtle's heading.
--
-- @tfield table LEFT
-- An empty table representing left, relative to the turtle's heading.
--
-- @tfield table RIGHT
-- An empty table representing right, relative to the turtle's heading.
--
-- @tfield {Vector,...} COMPASS_DIRS
-- A table containing NORTH, SOUTH, EAST, and WEST. These are all of the directions that can be found on a compass.
--
-- @tfield {Vector,...} PRIMITIVE_DIRS
-- A table containing UP, DOWN, LEFT, RIGHT, FORWARD, and BACK. These are all
-- of the directions that turtles have movement functions relating to.
--
-- @tfield {Vector,...} ABSOLUTE_DIRS
-- A table containing NORTH, EAST, SOUTH, WEST, UP, and DOWN. These are all of
-- the directions that aren't relative to the turtle's current heading. Also,
-- all of these directions are normalized Vectors.
--
local d = {
    -- absolute directions
    NORTH = Vector:new( 0,  0,  1),
    SOUTH = Vector:new( 0,  0, -1),
    EAST  = Vector:new( 1,  0,  0),
    WEST  = Vector:new(-1,  0,  0),
    UP    = Vector:new( 0,  1,  0),
    DOWN  = Vector:new( 0, -1,  0),

    -- relative directions
    -- because each of these is a different empty table, none of them are == to
    -- anything but themselves.
    FORWARD = {},
    BACK    = {},
    LEFT    = {},
    RIGHT   = {},
}

d.COMPASS_DIRS   = {d.NORTH, d.EAST, d.SOUTH, d.WEST}

d.PRIMITIVE_DIRS = {
    d.UP, d.DOWN, d.LEFT, d.RIGHT, d.FORWARD, d.BACK
}

d.ABSOLUTE_DIRS  = {
    d.NORTH, d.EAST, d.SOUTH, d.WEST, d.UP, d.DOWN
}

return d
