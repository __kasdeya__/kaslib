-------------------------------------------------------------------------------
-- A three-dimensional vector class.
--
-- Supports addition, subtraction, negation, multiplication, and division.
--
-- @module Vector
-- @author kasdeya
-- @license GNU GPLv3
-- @copyright kasdeya 2021

--- @type Vector
local Vector = {}
Vector.__index = Vector

--- Vector constructor.
--
-- @function Vector:new
-- @int[opt=0] x  The X coordinate of the vector.
-- @int[opt=0] y  The Y coordinate of the vector.
-- @int[opt=0] z  The Z coordinate of the vector.
-- @treturn Vector  The newly-created Vector instance.
function Vector.new(class, x, y, z)

    local self = {
        x=x or 0,
        y=y or 0,
        z=z or 0,
    }

    setmetatable(self, class)
    return self
end

function Vector:__tostring()
    return string.format('Vector(%s,%s,%s)', self.x, self.y, self.z)
end

function Vector:__unm()
    return Vector:new(-self.x, -self.y, -self.z)
end

function Vector.__eq(a, b)
    return (getmetatable(a) == getmetatable(b)
        and a.x == b.x
        and a.y == b.y
        and a.z == b.z)
end

--- Make a copy of the Vector.
--
-- @treturn Vector  The copy that was made.
function Vector:copy()
    return Vector:new(self.x, self.y, self.z)
end

--- Rotate along the yaw axis.
--
-- Returns a Vector rotated along the yaw axis in increments of 90 degrees
-- clockwise (positive numbers) or counter-clockwise (negative numbers).
--
-- @int degrees  The number of degrees to rotate the Vector by.
-- @treturn Vector  The rotated Vector.
-- @raise 'degrees must be divisible by 90!'
--
-- @usage
-- Vector:new(1,2,3):rotate(90)  --> Vector(3,2,-1)
-- Vector:new(1,2,3):rotate(180) --> Vector(-1,2,-3)
function Vector:rotate(degrees)
    degrees = degrees % 360

    if degrees == 0 then
        return self:copy()
    elseif degrees == 90 then
        return Vector:new(self.z, self.y, -self.x)
    elseif degrees == 180 then
        return Vector:new(-self.x, self.y, -self.z)
    elseif degrees == 270 then
        return Vector:new(-self.z, self.y, self.x)
    else
        error('degrees must be divisible by 90!')
    end
end

local function binaryOperatorMetamethodFactory(operationName, f)
    -- generates functions for binary operator metamethods like `__add` and
    -- `__mul`. you just need to tell it the name of the operation it's doing
    -- (for error messages) and give it a function `f` that looks something
    -- like:
    --
    -- function (a, b) return a - b end

    return function (a, b)
        if getmetatable(a) ~= Vector or getmetatable(b) ~= Vector then
            error('tried to perform ' .. operationName .. ' operation between Vector and non-Vector')
        end

        return Vector:new(f(a.x, b.x),
                          f(a.y, b.y),
                          f(a.z, b.z))
    end
end

Vector.__add  = binaryOperatorMetamethodFactory('addition',         function (a, b) return a + b end)
Vector.__sub  = binaryOperatorMetamethodFactory('subtraction',      function (a, b) return a - b end)
Vector.__mul  = binaryOperatorMetamethodFactory('multiplication',   function (a, b) return a * b end)
Vector.__div  = binaryOperatorMetamethodFactory('division',         function (a, b) return a / b end)
-- this lua version doesn't have integer division, I guess
-- Vector.__idiv = binaryOperatorMetamethodFactory('integer division', function (a, b) return a // b end)

return Vector
